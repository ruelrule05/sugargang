<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;

    public $fillable = ['title', 'price', 'quantity', 'image', 'ingredients', 'sku'];
    public $hidden = ['created_at', 'updated_at'];
}
