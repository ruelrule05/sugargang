<?php

namespace App\Http\Controllers;

use Goutte;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Jobs\GetProductDetails;

define('SITE', 'https://sugargang.com');

class ScraperController extends Controller
{
    public function index()
    {
        $url = SITE . '/collections/alle-produkte';
        // $url = SITE . '/collections/alle-produkte?page=26';

        Product::truncate();

        do {
            $crawler = Goutte::request('GET', $url);
            $crawler->filter('.product-item')->each(function($node) {
                GetProductDetails::dispatch($node->children('a')->attr('href'));
            });

            $nextLink = $crawler->filter('.pagination__next');

            if ($nextLink->count() > 0)
                $url = SITE . $nextLink->attr('href');
        } while($nextLink->count() > 0);

        return response()->json(['success' => true]);
    }
}
