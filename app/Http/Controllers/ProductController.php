<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->wantsJson())
        {
            // $search = $request->has('search');
            // $products = (new Product)->newQuery();
            // if ($request->has('search'))
            // {
            //     $search = $request->search;

            //     $products = $products->where('title', 'like', '%' . $search . '%')
            //                 ->orWhere('sku', 'like', '%' . $search . '%');

            // } else {
            //     $products = $products->limit(24);
            // }

            // if ($request->has('page'))
            // {
            //     $products = $products->offset(24 * $request->page);
            // }

            if ($request->has('search') && strlen($request->search) > 0)
            {
                $products = Product::where('title', 'like', '%' . $request->search . '%')
                                    ->orWhere('sku', 'like', '%' . $request->search . '%')
                                    ->offset((24 * $request->page) - 24)
                                    ->limit(24);
                $totalProducts = $products->count();
                $products = $products->get();
                        
            } else {
                $totalProducts = Product::count();
                $products = Product::offset((24 * $request->page) - 24)->limit(24)->get();
            }

            $currentPage = $request->has('page') ? $request->page : 1;
            $totalPages = ceil($totalProducts / 24);

    
            return response()->json([
                'current_page' => $request->has('page') ? $request->page : 1,
                'next_page_url' => $currentPage < $totalPages ? '/products?page=' . ($currentPage + 1) . '&search=' . $request->search : null,
                'prev_page_url' => $currentPage > 1 ? '/products?page=' . ($currentPage - 1) . '&search=' . $request->search : null,
                'last_page' => $totalPages,
                'data' => $products
            ]);
        } else {
            return Inertia::render('Welcome',[
                'page' => $request->has('page') ? $request->page : 1,
                'search' => $request->search
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return Inertia::render('ProductDetails', [
            'product' => $product
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return Inertia::render('EditProduct', [
            'product' => $product
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product->fill($request->all());
        
        return response()->json(['success' => $product->save()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function scraper(Request $request)
    {
        return Inertia::render('Scraper');
    }

    public function graph(Request $request)
    {
        $products = Product::all();

        $titles = [];
        $values = [];

        foreach ($products as $product) {
            // array_push($graphData, ['title' => $product->title, 'quantity' => $product->quantity]);
            array_push($titles, $product->title);
            array_push($values, $product->quantity);
        }

        return Inertia::render('Graph', [
            'titles' => $titles,
            'values' => $values
        ]);
    }
}
