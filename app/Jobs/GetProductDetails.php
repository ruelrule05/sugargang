<?php

namespace App\Jobs;

use Goutte;
use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

define('URL', 'https://sugargang.com');

class GetProductDetails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $link;
    protected $product;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($link)
    {
        $this->link = $link;
        $this->product = new \stdClass();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

            $crawler = Goutte::request('GET', URL . $this->link);
            $this->product->title = $crawler->filter('.product-meta__title')->text();
            $this->product->sku = $crawler->filter('.product-meta__reference')->children('span')->children('span')->text();
            $this->product->price = str_replace('Angebotspreis', '', $crawler->filter('.price')->text());
            $quantity = $crawler->filter('.product-form__inventory.inventory')->text();
    
            if (strpos($quantity, 'Nur noch') === false)
            {
                $firstIndex = strpos($quantity, '(');
                $lastIndex = strpos($quantity, ')');
                $this->product->quantity = substr($quantity, $firstIndex + 1, $lastIndex - (11 + $firstIndex));
            } else {
                $firstIndex = strpos($quantity, 'Nur noch');
                $lastIndex = strpos($quantity, 'auf Lager');
    
                $this->product->quantity = trim(substr($quantity, $firstIndex + 8, $lastIndex - (9 + $firstIndex)));
            }
            $imageSrc = $crawler->filter('.product-gallery__image')->attr('data-zoom');
            $this->product->image = substr($imageSrc, 2);
    
            $ingredientNodes = $crawler->filter('.rte.text--pull');
    
            if ($ingredientNodes->count() > 0)
            {
                $ingredient = $ingredientNodes->text();
    
                $stringIndex = strpos($ingredient, 'Inhaltsstoffe:');
                if ($stringIndex !== false)
                {
                    $this->product->ingredients = trim(substr($ingredient, ($stringIndex + 14)));
                } else {
                    $this->product->ingredients = $ingredient;
                }
            } else {
                $this->product->ingredients = '';
            }
    
            Product::create([
                'title' =>  $this->product->title,
                'sku' => $this->product->sku,
                'price' => str_replace(',','.',str_replace('€', '', $this->product->price)),
                'quantity' => $this->product->quantity,
                'ingredients' => $this->product->ingredients,
                'image' => $this->product->image
            ]);
        } catch (\Exception $e) {
            \Log::error($e);
        }

        // $product = Product::firstOrNew([
        //     'sku'   => $this->product->sku
        // ]);

        // $product->title = $this->product->title;
        // $product->price = str_replace(',','.',str_replace('€', '', $this->product->price));
        // $product->quantity = $this->product->quantity;
        // $product->ingredients = $this->product->ingredients;
        // $product->image = $this->product->image;

        // $product->save();
    }
}
